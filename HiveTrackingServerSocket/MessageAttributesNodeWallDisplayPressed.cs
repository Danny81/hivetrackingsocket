﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HiveTrackingServerSocket
{
    public class MessageAttributesNodeWallDisplayPressed
    {

        public long screenResolutionX  {get; set; }

        public long screenResolutionY { get; set; }

        public long devicePixelRatio { get; set; }

        public string nodeID { get; set; }

        public float x { get; set; }
        public float y { get; set; }

        public float xAbsolute { get; set; }

        public float yAbsolute { get; set; }

        public ScreenCoordinate graphComponentPosition { get; set; }

        public string detectionTime { get; set; }

        public MessageAttributesNodeWallDisplayPressed(long resX, long resY, long pxRatio, string node, float x, float y, float xAbs, float yAbs, ScreenCoordinate co,
            string detected)
        {
            screenResolutionX = resX;
            screenResolutionY = resY;
            devicePixelRatio = pxRatio;
            nodeID = node;
            this.x = x;
            this.y = y;
            xAbsolute = xAbs;
            yAbsolute = yAbs;
            graphComponentPosition = co;
            detectionTime = detected;
        }

    }
}
