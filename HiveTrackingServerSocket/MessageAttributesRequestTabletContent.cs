﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HiveTrackingServerSocket
{
    public class MessageAttributesRequestTabletContent
    {
        public float relativePosOnScreenX { get; set; }
        public float relativePosOnScreenY { get; set; }

        public long detectedTime { get; set; }

        public MessageAttributesRequestTabletContent(float posRelX, float posRelY, long whichTime)
        {
            relativePosOnScreenX = posRelX;
            relativePosOnScreenY = posRelY;
            detectedTime = whichTime;
        }

    }
}
