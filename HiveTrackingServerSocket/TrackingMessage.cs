﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HiveTrackingServerSocket
{
    public class TrackingMessage
    {
        public string fromDeviceID { get; set; }

        public string toDeviceID { get; set; }

        public Guid messageID { get; set; }

        public Constants.TrackingMessageTypes  messageType { get; set; }

        public MessageAttributesNodeWallDisplayPressed NodeWallDisplayPressedAttributes { get; set;}

        public MessageAttributesShowNodeDetails showNodeDetailsAttributes { get; set; }

        public MessageAttributesShowTabletContentAttributes showTabletContentAttributes { get; set; }

        public MessageAttributesRequestTabletContent requestTabletContentAttributes { get; set; }

        private TrackingMessage()
        {
            messageID = Guid.NewGuid();
        }

        public TrackingMessage(string fromDevice, string toDevice, Constants.TrackingMessageTypes msgType) : this()
        {
            fromDeviceID = fromDevice;
            toDeviceID = toDevice;
            messageType = msgType;
        }

        public TrackingMessage(string fromDevice, string toDevice, Constants.TrackingMessageTypes msgType, float relPosX, float relPosY, long detectedTime)
            : this(fromDevice, toDevice, msgType)
        {
            MessageAttributesRequestTabletContent attributes = new MessageAttributesRequestTabletContent(relPosX, relPosY, detectedTime);
            requestTabletContentAttributes = attributes;
        }

    }
}
