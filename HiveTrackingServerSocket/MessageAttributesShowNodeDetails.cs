﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HiveTrackingServerSocket
{
    public class MessageAttributesShowNodeDetails
    {

        public string nodeID { get; set; }
        public string detectionTime { get; set; }

        public MessageAttributesShowNodeDetails(string node, string detected)
        {

            nodeID = node;
            detectionTime = detected;
        }

    }
}
