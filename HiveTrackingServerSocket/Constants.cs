﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HiveTrackingServerSocket
{
    public static class Constants
    {

        public enum TrackingMessageTypes 
        {
            InitConnection,
            Identify,
            Deregister,
            NodePressedWallDisplay,
            ShowNodeDetails,
            RequestTabletContent,
            SendTabletContent,
            ProjectTabletContent
        }

    }
}
