﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace HiveTrackingServerSocket
{
    public class TrackingLogger
    {

        private string loggingConsole;

        public TrackingLogger(string logMode)
        {
            this.loggingConsole = logMode;
        }

        public void log(string logMessage)
        {
            if(loggingConsole.Equals("standalone"))
            {
                Console.WriteLine(logMessage);
            }

            else
            {
                Debug.Log(logMessage);
            }
        }

        public void log(string logMessage, object arg0, object arg1)
        {
            if (loggingConsole.Equals("standalone"))
            {
                Console.WriteLine(logMessage, arg0, arg1);
            }

            else
            {
                Debug.Log(string.Format(logMessage, arg0, arg1));
            }
        }

        public void log(string logMessage, object arg0)
        {
            if (loggingConsole.Equals("standalone"))
            {
                Console.WriteLine(logMessage, arg0);
            }

            else
            {
                Debug.Log(logMessage);
            }
        }

    }
}
