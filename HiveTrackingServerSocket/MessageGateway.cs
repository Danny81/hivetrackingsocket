﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WebSocketSharp;
using WebSocketSharp.Server;

namespace HiveTrackingServerSocket
{
    public class MessageGateway : WebSocketBehavior
    {

        public TrackingLogger log;
        public TrackingServerSocket hostingSocket;

        public MessageGateway(TrackingLogger logger, TrackingServerSocket socket)
        {
            log = logger;
            hostingSocket = socket;
        }

        protected override void OnMessage(MessageEventArgs e)
        {

            try
            {
                TrackingMessage tm = JSONHelper.ToTrackingMessage(e.Data);
                string reply = "";

                switch (tm.messageType)
                {
                    case Constants.TrackingMessageTypes.InitConnection:

                        reply = "[TrackingServerSocket]: Initialization message received...";
                        Send(reply);
                        break;

                    case Constants.TrackingMessageTypes.Identify:

                        hostingSocket.devices.AddOrUpdateDevice(tm.fromDeviceID, ID);
                        log.log("Registered device " + tm.fromDeviceID + " with session ID " + ID);
                        reply = "[TrackingServerSocket]: Identify message received...";
                        Send(reply);
                        break;

                    case Constants.TrackingMessageTypes.Deregister:

                        hostingSocket.devices.RemoveDevice(tm.fromDeviceID);
                        reply = "[TrackingServerSocket]: Deregister message received...";
                        Send(reply);
                        break;

                    case Constants.TrackingMessageTypes.NodePressedWallDisplay:
                        string str = "WALL DISPLAY NODE PRESSED";
                        // nofify Unity so that it can try to look fpr the nearest tracker
                        NodeWallDisplayPressedEventArgs pressedEvent = new NodeWallDisplayPressedEventArgs(tm);
                        hostingSocket.OnNodeOnWallDisplayPressed(this, pressedEvent);
                        reply = "[TrackingServerSocket]: NodePressedWallDisplay message received...";
                        Send(reply);
                        break;
                    case Constants.TrackingMessageTypes.SendTabletContent:
                        log.log("Received image data for wall screen: " + tm.showTabletContentAttributes.dataUrl);
                        reply = "[TrackingServerSocket]: SendTabletContent message received...";
                        Send(reply);

                        //forward content to Wall Display
                        var sessionWallDisplay = hostingSocket.devices.TryGetSessionIdFromDevice("WallDisplay");
                        if (sessionWallDisplay != null)
                        {
                            var forwardedMessage = JSONHelper.ToJSON(tm);
                            log.log("[TrackingServerSocket]: sending " + forwardedMessage);
                            hostingSocket.webSocket.WebSocketServices["/trackinggateway"].Sessions.SendTo(forwardedMessage, sessionWallDisplay);
                        }
                        else
                        {
                            log.log("Wall Display not registered in Socket - cannot forward tablet content");
                        }

                        break;
                    default:
                        log.log("Unknown message type");
                        reply = "[TrackingServerSocket]: unknown message received...";
                        Send(reply);
                        break;
                }


            }
            catch (JsonReaderException ex)
            {
                log.log("No JSON data");
                log.log(ex.ToString());
                log.log(ex.StackTrace);
            }

        }

        public void SendToDevice(string deviceID, string message)
        {

            if (!hostingSocket.devices.connectedDevices.ContainsKey(deviceID))
            {
                log.log("WARNING : device {0} not registered, no message will be sent", deviceID);
                return;
            }

            Sessions.SendTo(message, hostingSocket.devices.connectedDevices[deviceID]);
        }

    }
}
