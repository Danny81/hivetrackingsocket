﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace HiveTrackingServerSocket
{
    public class DevicesManager
    {

        public Dictionary<string, string> connectedDevices;
        private TrackingLogger logger;

        public DevicesManager(TrackingLogger log)
        {
            logger = log;
            connectedDevices = new Dictionary<string, string>();
        }

        public void AddOrUpdateDevice(string device, string sessionID) 
        {
            if (!connectedDevices.ContainsKey(device))
            {
                connectedDevices.Add(device, sessionID);
            }
            else
            {
                connectedDevices[device] = sessionID;
            }
        }

        public void RemoveDevice(string device)
        {
            if (connectedDevices.ContainsKey(device))
            {
                connectedDevices.Remove(device);
                logger.log("Removed device " + device + ", " + connectedDevices.Keys.Count + " remaining connected...");
            }
            else
            {
                logger.log("Device " +device + " has no registered thread, nothing to do...");
            }
        }

        public string TryGetSessionIdFromDevice(string deviceID)
        {
            if (connectedDevices.ContainsKey(deviceID))
            {
                return connectedDevices[deviceID];
            }
            else
            {
                return null;
            }
        }

        /*sends a ShowNodeDetailsMessage to the device - used to initiate node details view in browser*/
        public string BuildMsgShowNodeDetailsToDevice(string fromDevice, string toDevice, string nodeID, string detectionTime)
        {
            TrackingMessage tm = new TrackingMessage(fromDevice, toDevice, Constants.TrackingMessageTypes.ShowNodeDetails);
            MessageAttributesShowNodeDetails attributes = new MessageAttributesShowNodeDetails(nodeID, detectionTime);
            tm.showNodeDetailsAttributes = attributes;
            string message = JSONHelper.ToJSON(tm);
            return message;
        }

        public string BuildMsgRequestTabletContentFromDevice(string toDevice, float relativeScreenPosX, float relativeScreenPosY, long detectedTime)
        {
            TrackingMessage tm = new TrackingMessage("socket", toDevice, Constants.TrackingMessageTypes.RequestTabletContent, relativeScreenPosX, relativeScreenPosY, detectedTime);
            string message = JSONHelper.ToJSON(tm);
            return message;
        }

    }
}
