﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Configuration;
using System.Net.WebSockets;
using System.Xml;
using System.Threading;
using Newtonsoft.Json;
using WebSocketSharp.Server;

namespace HiveTrackingServerSocket
{
    public class TrackingServerSocket
    {

        private TrackingLogger log;

        private XmlDocument config;

        private int listeningPort;
        private IPAddress serverSocketIP;
        public WebSocketServer webSocket;
        public MessageGateway msgGateway;

        public DevicesManager devices;

        public delegate void NodeWallDisplayPressed(object sender, NodeWallDisplayPressedEventArgs e);
        public event NodeWallDisplayPressed NodeOnWallDisplayPressed;


        public static TrackingServerSocket Instance
        {
            get { return lazy.Value; }
        }

        private static readonly Lazy<TrackingServerSocket> lazy = new Lazy<TrackingServerSocket>(() => new TrackingServerSocket());

        private TrackingServerSocket()
        {

            config = new XmlDocument();
            config.Load("socketConfig.xml");
            listeningPort = Convert.ToInt32(config.SelectSingleNode("/config/serversocketport").InnerText);
            log = new TrackingLogger(config.SelectSingleNode("/config/loggingconsole").InnerText);
            serverSocketIP = IPAddress.Parse(config.SelectSingleNode("/config/serversocketip").InnerText);
            webSocket = new WebSocketServer(serverSocketIP, listeningPort);
            devices = new DevicesManager(log);
            webSocket.AddWebSocketService<MessageGateway>("/trackinggateway", () => new MessageGateway(log, this));
        }

        /*private string GetIPAddress()
        {
            IPHostEntry host;
            string localIP = "";
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    localIP = ip.ToString();
                }

            }
            return localIP;
        }*/

        public void StartListener()
        {

            //this.server.Start();

            webSocket.Start();
            while (true)
            {

            }

            /*try
            {
                while (true)
                {
                    log.log("Waiting for a connection...");
                    TcpClient client = server.AcceptTcpClient();
                    log.log("Connected!");
                    Thread t = new Thread(new ParameterizedThreadStart(HandleDevice));
                    t.Start(client);
                }
            }
            catch (SocketException e)
            {
                Console.WriteLine("SocketException: {0}", e);
                server.Stop();
            }*/
        }

        public virtual void OnNodeOnWallDisplayPressed(object sender, NodeWallDisplayPressedEventArgs e)
        {
            NodeOnWallDisplayPressed?.Invoke(this, e);
        }


        /*public void HandleDevice(object obj)
        {
            TcpClient client = (TcpClient)obj;
            bool keepReading = true;
            Byte[] bytes = new Byte[1000000];

            try
            {
                while (keepReading) {
                    client = (TcpClient)obj;

                    var stream = client.GetStream();

                    string data = null;
                    
                    int i;            

                    while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
                    {
     
                        data = Encoding.UTF8.GetString(bytes, 0, i);
                        log.log("{1}: Received (raw): {0}", data, Thread.CurrentThread.ManagedThreadId);                      

                        bool isWebSocketHandshake = wsUtilities.CheckHandshake(data, stream);
                        if(isWebSocketHandshake)
                        {
                            devices.checkAddThread(Thread.CurrentThread.ManagedThreadId);
                            devices.SendToDevice(Thread.CurrentThread.ManagedThreadId, stream, "[TrackingServerSocket]: got your WS handshake");
                        }

                        if (!isWebSocketHandshake)
                        {
                            data = wsUtilities.DecodeIncomingMessage(bytes, 0, i);
                            try
                            {
                                TrackingMessage tm = JSONHelper.ToTrackingMessage(data);
                                HandleIncomingMessage(tm, Thread.CurrentThread.ManagedThreadId, stream);
                                string reply = "[TrackingServerSocket]: got your " + tm.messageType + " message";
                                if (!String.IsNullOrEmpty(tm.fromDeviceID) && tm.messageType!=Constants.TrackingMessageTypes.Deregister)
                                {
                                    devices.SendToDevice(tm.fromDeviceID, reply);
                                }
                                else
                                {
                                    devices.SendToDevice(Thread.CurrentThread.ManagedThreadId, stream, reply);
                                }
                                
                            }
                            catch (JsonReaderException e)
                            {
                                log.log("Invalid JSON data received");
                            }
                        }
                        
                    }
                    System.Threading.Thread.Sleep(1);

                }
                log.log("Done with device " + Thread.CurrentThread.ManagedThreadId);
  
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: {0}", e.ToString());
                client.Close();
            }

        }*/

    }
}
