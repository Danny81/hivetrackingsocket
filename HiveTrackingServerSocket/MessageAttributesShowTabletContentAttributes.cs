﻿namespace HiveTrackingServerSocket
{
    public class MessageAttributesShowTabletContentAttributes
    {
        public string dataUrl { get; set; }

        public MessageAttributesShowTabletContentAttributes(string imgData)
        {

            dataUrl = imgData;

        }
    }
}